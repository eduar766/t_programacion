function alea(minimo,maximo){
  var numero= Math.floor(Math.random()*(maximo-minimo+1)+minimo);
  return numero;
}

var piedra=0;
var papel=1;
var tijera=2;
var lagarto=3;
var spock=4;

var opciones=["piedra", "papel", "tijera", "lagarto", "spock"];

var opcionUsuario;
var opcionMaquina= alea(0,4);

opcionUsuario= prompt("¿Cual opcion eliges?\nPiedra=0\nPapel=1\nTijeras=2\nLagarto=3\nSpock=4", "0");

alert("Elegiste "+opciones[opcionUsuario]);
alert("Javascript eligio " +opciones[opcionMaquina]);

if(opcionUsuario == piedra){
  if(opcionMaquina == piedra){
    alert("Ups... empate, elige mejor la proxima vez.");
  }
  if(opcionMaquina == papel){
    alert("El papel cubre a la piedra... ¡Perdiste!");
  }
  if(opcionMaquina == tijera){
    alert("Ganaste");
  }
  if(opcionMaquina == lagarto){
    alert("La piedra aplasta al lagarto... ¡Ganaste!");
  }
  if(opcionMaquina == spock){
    alert("Spock vaporiza la piedra... ¡Perdiste!");
  }
}
else if(opcionUsuario == papel){
  if(opcionMaquina == piedra){
    alert("El papel cubre a la piedra... ¡Ganaste!");
  }
  if(opcionMaquina == papel){
    alert("Ups... empate, elige mejor la proxima vez.");
  }
  if(opcionMaquina == tijera){
    alert("Las tijeras cortan el papel... ¡Perdiste!");
  }
  if(opcionMaquina == lagarto){
    alert("El lagarto se come el papel... ¡Perdiste!");
  }
  if(opcionMaquina == spock){
    alert("El papel refuta a Spock... ¡Ganaste!");
  }
}
else if(opcionUsuario == tijera){
  if(opcionMaquina == piedra){
    alert("¡Perdiste!");
  }
  if(opcionMaquina == papel){
    alert("Las tijeras cortan el papel... ¡Ganaste!");
  }
  if(opcionMaquina == tijera){
    alert("Ups... empate, elige mejor la proxima vez.");
  }
  if(opcionMaquina == lagarto){
    alert("Las tijeras decapitan al lagarto... ¡Ganaste!");
  }
  if(opcionMaquina == spock){
    alert("Spock destroza las tijeras... ¡Perdiste!");
  }
}
else if(opcionUsuario == lagarto){
  if(opcionMaquina == piedra){
    alert("La piedra aplasta al lagarto... ¡Perdiste!");
  }
  if(opcionMaquina == papel){
    alert("El lagarto se come el papel... ¡Ganaste!");
  }
  if(opcionMaquina == tijera){
    alert("Las tijeras decapitan al lagarto... ¡Perdiste!");
  }
  if(opcionMaquina == lagarto){
    alert("Ups... empate, elige mejor la proxima vez.");
  }
  if(opcionMaquina == spock){
    alert("El lagarto envenena a Spock... ¡Ganaste!");
  }
}
else if(opcionUsuario == spock){
  if(opcionMaquina == piedra){
    alert("Spock vaporiza la piedra... ¡Ganaste!");
  }
  if(opcionMaquina == papel){
    alert("El papel refuta a Spock... ¡Perdiste!");
  }
  if(opcionMaquina == tijera){
    alert("Spock destroza las tijeras... ¡Ganaste!");
  }
  if(opcionMaquina == lagarto){
    alert("El lagarto envenena a Spock... ¡Perdiste!");
  }
  if(opcionMaquina == spock){
    alert("Ups... empate, elige mejor la proxima vez.");
  }
}
else {
  alert("Elige una opción correcta, porfavor dejate de tonterias");
}
