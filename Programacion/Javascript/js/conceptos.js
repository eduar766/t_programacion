//Estructura de una funcion.
var saludo = function (nombre) {
    console.log("Qué bueno verte," + " " + nombre);
};
saludo("Eduardo");

//----------------------------------------------------------------------------//
var perimetroCaja = function (longitud, ancho){
    return longitud+longitud+ancho+ancho;  //return devuelve el valor de la funcion y esta se puerde asinar a otra variable
    };

perimetroCaja(1,1);

var caja = perimetroCaja();
//----------------------------------------------------------------------------//
//                             Ciclos for

for (var i = 1; i < 11; i = i + 1){ //Ciclo for, tiene 3 partes: Donde empieza, donde termina y lo que hace para iterar.
	console.log(i);
};

for (var i = 5; i < 51; i += 5) { // este ciclo cuenta de 5 en 5. Para ello, la condicion de iteracion de be ser += 5.
	console.log(i);
}
//----------------------------------------------------------------------------//
//                   Ciclos "for" con Arrays.

var ciudades = ["Misiones", "Salta", "Mendoza", "Buenos Aires", "El Tigre", "Caracas"]; //los arrays se declaran entre []

for (var i = 0; i < ciudades.length; i++) {   //lo que hace este ciclo es obtener la longitud del array ciudades y lo coloca como condicion final del ciclo for
    console.log("Me gustaría visitar" + " " + ciudades[i]); //luego se almacena en la variable i y se muestra.
}
//----------------------------------------------------------------------------//
//                    Ciclos While.

var hola = true;
var soloCiclo = function(){
  while (hola){
      console.log("¡Un ciclo!");
      hola = false;
  } 
};

soloCiclo();
//----------------------------------------------------------------------------//

var condicion = false;
do {
    console.log("Me muestran una vez");
} while(condicion);

//----------------------------------------------------------------------------//
// Juego que consiste en matar Dragones.

var atacar = true;
var vosAcertas = Math.floor(Math.random() * 2); //Asigna un valor de 0 (false) o de 1 (true)
var lastimaEsteRound = Math.floor(Math.random()*5 + 1); //Asigna un numero aleatorio entre 1 y 5 (5 incluido)
var perdidaTotal = 0;

while(atacar){ // Verificamos que se pueda atacar siempre que atacar sea true.
    if (vosAcertas){
        console.log("¡Vos acertás!");
        perdidaTotal += lastimaEsteRound; // es lo mismo que perdidaTotal = perdidaTotal + lastimaEsteRound
        if (perdidaTotal >= 4){
            console.log("You win");
            atacar = false;
            } else{
                vosAcertas = Math.floor(Math.random()*2);
                }
        } else {
            console.log("¡Le Erraste!");
            };
    atacar = false;
    };