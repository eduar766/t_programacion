var tablero;
var fondo= {
	imagenURL:"fondo.png",
    imagenOK:false
};

var tifis ={
    x:100,
    y:100,
    frenteURL:"diana-frente.png",
    frenteOK:false
};

function inicio(){
    var canvas = document.getElementById("campo");
    tablero = canvas.getContext("2d");

    fondo.imagen = new Image();
    fondo.imagen.src = fondo.imagenURL;
    fondo.imagen.onload = confirmarFondo;

    tifis.imagen = new Image();
    tifis.imagen.src = tifis.imagenURL;
    tifis.imagen.onload = confirmarFondo;
}

function confirmarFondo(){
	fondo.imagenOK=true;
    dibujar();
}

function dibujar(){
    if(fondo.imagenOK==true){
        tablero.drawImage(fondo.imagen,0,0);
    }
    if(tifis.frenteOK==true){
        tablero, drawImage(tifis.frente, 0, 0);
    }
}