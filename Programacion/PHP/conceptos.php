<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h2>
		<?php
		echo "Hola Que tal?"; //Cadena de Texto
		echo "Hola "."Que tal ". "?"; //Para concatenar se utiliza el simbolo .
		echo 5+3;
		//echo sirve para mostrar un resultado directamente en la pagina.
		?>
	</h2>

	<p>
		<?php
		$variable="Nombre de la variable";
		//con el operador $ se establecen las variables.
		?>
	</p>

	<p>
		<?php //      Condicional if
		 $edad = 17;

		 if( $edad > 16 ) {
		 	echo "¡Podés manejar!";
		 }
		 //Ejemplo del condicionante if.
		?>
	</p>

	<p>
		<?php //          Arrays
		$decenas = array(10, 20, 30, 40, 50);
        echo $decenas[2] //Muestra el segundo valor del array
        //asi se trabajan los arrays. La primera posicion empieza desde 0.
        $decenas[2]= 60; //Asi se cambia el valor de una posicion del array.
        unset($decenas[3]); //Elimina la tercera variable del array.
		?>
	</p>

	<p>
		<?php //         Ciclo for.
		//El ciclo for se usa cuando ya conocemos el numero de veces que se debe repetir la iteracion
		for($i = 0; $i < 101; $i = $i + 10) { //Son 3 condiciones que se establecen (Donde inicia ; Donde termina ; Que hacepara iterar el ciclo)
          echo $i; //Muestra el resultado del ciclo.

          // 			 Ciclo foreach.
          // El ciclo foreach se usa cuando queremos mostrar los elementos de un array uno por uno.

        $jugadaDeFutbol = array("primer pase... ", "segundo pase... ",
        "centro al área... ", "cabezaaa... ");

        foreach ($jugadaDeFutbol as $jugada){ //Cada elemento del array se almacena temporalmente en una variable y se muestra.
            echo $jugada;
        };
        
        echo "¡Gooooool!";
		?>
	</p>

	<p>
		<?php //           Ciclo While.
		$hola = 0;
		while ($hola < 5 ){ //Se coloca la condicion a la que se evalua la variable para luego ejecutar la accion, y la iteración.
        echo "Mi primer ciclo while: {$hola}";
        $hola ++;

        //                 Ciclo do-while
        $conteo = 0;
        do {
        	echo $conteo . "Esto se contara 1 vez al menos";
            }
            while ($conteo < 3){
                echo "Callate wn";
                $conteo ++;
            }
        }
    ?>
		?>
	</p>

	<p>
		<?php
		?>
	</p>

	<p>
		<?php
		?>
	</p>

	<p>
		<?php
		?>
	</p>
</body>
</html>