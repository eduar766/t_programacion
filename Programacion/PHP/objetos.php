<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

<p>
	<?php // Creando un Objeto
	//Con class se crea el objeto Persona
	class Persona{
            public $estaViva = true; //Con public se crean las propiedades del objeto Persona, pueden tener propiedades predefinidas
            						 // o se le pueden pasar atributos mediante el constructor new.
            public $nombre;
            public $apellido;
            public $edad;
            }

        $maestro = new Persona();  // Con new, se crea una nueva instancia de Persona y se asigna a $maestro, $estudiante, etc.
        $estudiante = new Persona();
        
        echo $maestro -> estaViva;  //Para ver/usar un atributo o propiedad de un objeto se hace asi: $nombreDelObjeto -> propiedad
	?>
</p>

<p>
	<?php
	        class Persona {
            public $estaViva = true;
            public $nombre;
            public $apellido;
            public $edad;
            
            public function __construct($nombre, $apellido, $edad){   // Con function __construct($prop1, $prop2) lo que logramos es crear el metodo mediante el cual, se puedan construir los objetos mas adelantes, y a su vez, pasarle los datos.
                $this -> nombre = $nombreDePila;
                $this -> apellido = $apellido;
                $this -> edad = $edad;
                }
            public function saludo(){    //Asi se crean los metodos.
            	return "Hola, mi nombre es ". $this -> nombre. " " . $this -> apellido . " " . "¡Encantado de conocerte :-)"; 
            }
            
            $maestro = new Persona("aburrida", "12345", 12345); //Acá se muestra como se les puede pasar los datos que conforman el objeto.
            $estudiante = new Persona("Eduardo", "Saavedra", 26);
            
            echo $estudiante -> edad;
            echo $estudiante -> saludo(); 
	               
	?>
</p>

<p>
	<?php
	        class Persona {
          public $nombre = "";
          
          function __construct($nombre) {
              $this->nombre = $nombre;
          }
          
          public function bailar() {
            return "Estoy bailando";
          }
        }
        
        $yo = new Persona("Sabrina");
        if (is_a($yo, "Persona")) {   // Pregunta si la variable que se esta definiendo es un objeto
          echo "Soy una persona, ";
        }
        if (property_exists($yo, "nombre")) {  // Pregunta si existe alguna propiedead, como por ejemplo, el nombre.
          echo "Tengo un nombre, ";
        }
        if (method_exists($yo, "bailar")) {   // pregunta si hay o existe algun metodo en especifico.
          echo "y se bailar.";
        }
	?>
</p>

<p>
	<?php   // Herencia.
	class Forma {
          public $tieneLados = true;
        }
        // extends lo que hace es indicar que Cuadrado es un "hijo" de Forma, porque cuadrado es una forma.. por ende debe tener las mismas propiedades Heredadas. 
        class Cuadrado  extends Forma {  
        }   
        $cuadrado = new Forma();
        if (property_exists ($cuadrado, "tieneLados") ) {  //Se pregunta si Cuadrado tiene la propiedad "tieneLados"
          echo "Tengo lados.";
        }
	?>
</p>

<p>
	<?php
	class Vehiculo {
          final public function bocinazo() {   //la palabra clave final le indica al programa hasta cuando los hijos pueden modificar los otributos de la clase padre
            return "HONK HONK!";
          }
        } 
        
        class Bicicleta extends Vehiculo {
           public function bocinazo() {
                return "¡Pip Pip!";
                }
            }
            
        $bicicleta = new Bicicleta();
        
        echo $bicicleta -> bocinazo();
	?>
</p>

<p>
	<?php
	//   Variables constantes.
	      class Persona { 
      }
      class Ninja extends Persona {
        const stealth = "MAXIMO"; // const se usa para declarar que una variable es constante... no se usa $ en este tipo de variables.
      }
      echo Ninja::stealth;  //Para hacer un llamado a una variable constante se hace de este modo, con ::
      
	?>
</p>

<p>
	<?php
	class Rey {
		//      static se usa en las variables de l objeto que requieran ser usadas directamente y se llaman a traves de ::
          static function anunciar() {
            echo "Un anuncio real";
          }
        }
        echo Rey::anunciar();
	?>
</p>

<p>
	<?php
	$amor = array('pelo' => 'negro',
                  'labios' => 'rojos',
                  'sonrisa' => 'radiante');
    
    foreach ($amor as $caracteristicas => $color){
        echo 'Yo amo tu '.$caracteristicas. ' ' . $color;
        }

	?>
</p>

<p>
	<?php
	?>
</p>

<p>
	<?php
	?>
</p>




</body>
</html>