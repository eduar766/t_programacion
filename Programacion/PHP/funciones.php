<p>
		<?php           //Funciones de cadena

		$largo = strlen("Eduardo Saavedra"); // strlen() Mide la cantidad de caracteres de una cadena.
		print $largo;

		// Funcion substr 
		 $miNombre = "Eduardo";
   		 $parcial = substr($miNombre, 0, 5); //Almacena una parte de una cadena. Se coloca el nombre del array, donde empieza, donde termina.
    	 print $parcial;

    	 // funcion strtoupper
    	 print strtoupper($miNombre); // Cambia todas las letras del array a Mayusculas.

    	 // funcion strtolower
    	 print strtolower($miNombre); // Cambia todas las letras del array a minusculas.

    	 // Muestra la posicion de una letra en una cadena
    	 $h = strpos ("Eduardo", "d");
    	 print $h;

    	 //Muestra un valor errado cuando no se encuentra una letra en una cadena de texto.
    	 if (strpos("david","h") === false) {
    	 	print "Disculpá, no hay 'h' en 'david'";
    	 }

		?>
	</p>

		<p>
		<?php   // Funciones Matematicas.

		// round redondea hacia abajo un valor de coma flotante, M_PI es el numero PI, una constante en php.
		$redondear = round(M_PI);
		print $redondear;

		$redondear_decimal = round (M_PI , 3); // a la funcion round se le puede asignar un segundo parametro que indique la cantidad de decimales que se requieren.
  	    print $redondear_decimal;

  	    // rand() Escoje un numero aleatorio entre 2 parametros (min, max)
  	    print rand(0,10);

  	    //array_push es una funcion que permite agregar valores en la parte final de un array, para de esta manera aumentar el tamaño.
  	    $bandas = array("pink floyd", "led zepelling", "The cure");
    	array_push($bandas, "Guns n roses");
    	array_push($bandas, "Radiohead");
    
    	print count($bandas);

    	// ordenar arrays
    	$objetos = array(5,2,41,9,8,7);
    	sort($objetos);     //Ordena los arrrays de menor a mayor
    	rsort($objetos);	//Ordena los arrays de mayor a menor
    	print join("," , $objetos);  //join se usa para establecer el elemento separador entre cada componente del array.
	?>
		?>

	</p>

		<p>
		<?php
		// Creá un array e insertá los nombres
    	// de tus familiares y amigos cercanos
    	$familia = array("Neyda", "Eduardo", "Eneydir", "Daniel", "Cristobal", "Irma");
    
		// Ordená la lista
    	sort($familia);
		// En forma aleatoria elegí al ganador
    	$eleccion=rand(0,count($familia)-1);
    	$ganador=strtoupper($familia[$eleccion]);
		// Mostrá el nombre del ganador en MAYÚSCULAS
		print $ganador;
		?>
	</p>

	<p>
		<?php //     Funciones...
		 function sobreMi($nombre , $edad){  // function declara la funcion... Nombre de la funcion y parametros de la misma.
          echo "¡Hola! Mi nombre es " . $nombre . ", y tengo " . $edad . " años.";
          }
          
          sobreMi("Eduardo", 26);  // al escribir el nombre de la funcion y asignarles parametros, se hace un llamado a dicha funcion.
		?>
	</p>